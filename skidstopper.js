while (true) {
    if (window.self !== window.top) {
        var parentUrl = document.referrer;

        if (parentUrl.startsWith("https://4kh0-revolt.oy.lc")) {
            console.log("We're good!");
        } else {
            document.body.innerHTML = ''; // Remove everything on the page
            document.write('<h1 style="color: white; background-color: red;">You seem to be using 4kh0 games without permission. Please ask LegionDev for permission before using.</h1>');
            console.log("I'm sad");
            // Reload the script
            setTimeout(function() {
                location.reload();
            }, 5000); // Reload after 5 seconds (adjust the delay as needed)
        }
    } else {
        console.log("This page is not being iframed. All good.");
    }
}
